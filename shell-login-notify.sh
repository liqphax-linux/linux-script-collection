#!/bin/bash
############################################################
# Usage: Call this script at the end of /etc/profile.
############################################################
echo -e "Login auf $(hostname) am $(date +%Y-%m-%d) um $(date +%H:%M) UTC\nBenutzer: $USER" | mailx -s "Example-Server: Shell Login Notification" -r "Example-Server <example@example.com>" someuser@example.com